
import { NavLink } from "react-router-dom"

function Nav() {
    return (
        <div className = "container">
            <header>
                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                <div className="container-fluid">
                    <a className="navbar-brand" href="#">Conference GO!</a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                            <li className="nav-item">
                                <a className="nav-link"  href="#">Home</a>
                            </li>
                            <NavLink to="/locations/new" className="nav-link" aria-current="page">New location</NavLink>
                            <NavLink to="/conferences/new" className="nav-link" aria-current="page">New conference</NavLink>
                            <NavLink to="/presentations/new" className="nav-link" aria-current="page">New presentation</NavLink>
                        </ul>
                    </div>
                </div>
                </nav>
            </header>
        </div>
    );
};

export default Nav;