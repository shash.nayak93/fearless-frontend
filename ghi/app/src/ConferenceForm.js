import React, { useEffect, useState } from 'react';

function ConferenceForm() {
    const [locations,setLocations] = useState([]);
    const [name, setName] = useState('');
    const [start, setStart] = useState('');
    const [end, setEnd] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [location, setLocation] = useState('');
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
          const data = await response.json();
          setLocations(data.locations);
          }
        }

      useEffect(() => {
        fetchData();
      }, []);

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleStartChange = (event) => {
        const value = event.target.value;
        setStart(value);
    }

    const handleEndChange = (event) => {
        const value = event.target.value;
        setEnd(value);
    }

    const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
    }

    const handleMaxPresentationsChange = (event) => {
        const value = event.target.value;
        setMaxPresentations(value);
    }

    const handleMaxAttendeesChange = (event) => {
        const value = event.target.value;
        setMaxAttendees(value);
    }

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty JSON object
        const data = {};

        data.name = name;
        data.starts = start;
        data.ends = end;
        data.description = description;
        data.max_presentations=maxPresentations;
        data.max_attendees=maxAttendees;
        data.location=location;
        console.log(data);

        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const conferenceResponse = await fetch(conferenceUrl, fetchConfig);
        if (conferenceResponse.ok) {
            const newConference = await conferenceResponse.json();
            console.log(newConference);

            setName('');
            setStart('');
            setEnd('');
            setDescription('');
            setMaxPresentations('');
            setMaxAttendees('');
            setLocation('');

        }
      }

    return(
        <>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" id="name" name="name" value={name} className="form-control" />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartChange} placeholder="Starts" required type="date" id="starts" name="starts" value={start} className="form-control" />
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndChange} placeholder="Ends" required type="date" id="ends" name="ends" value={end} className="form-control" />
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="mb-3">
                <label htmlFor="description" className="form-label">Description</label>
                <textarea onChange={handleDescriptionChange} className="form-control" name="description" id="description" value={description} rows="3"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPresentationsChange} placeholder="Maximum Presentations" required type="text" id="max_presentations" name="max_presentations" value={maxPresentations} className="form-control" />
                <label htmlFor="max_presentations">Maxiumum Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAttendeesChange} placeholder="Maximum Attendees" required type="text" id="max_attendees" name="max_attendees" value={maxAttendees} className="form-control" />
                <label htmlFor="max_attendees">Maxiumum Attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={handleLocationChange} required id="location" name="location" value={location} className="form-select">
                  <option value="">Choose location</option>
                    {locations.map(location => {
                        return (
                            <option key = {location.id} value={location.id}>
                                {location.name}
                            </option>
                        );
                    })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      </>
    );
}

export default ConferenceForm;